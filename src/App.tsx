import React, { Suspense } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { useStyles } from "./Styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Menu from "./component/menu/Menu";
import About from "./component/about/About"
import Topics from "./component/topics/Topics"
import Photo from "./component/photo/Photo"
import Swiper from "./component/swiper/Swiper"
import Home from "./component/home/Home"
import Login from "./component/login/Login";
import { AppProvider, useAppState } from "./context"

const List = React.lazy(() => import("./component/list/List"));

const LoginRender = () => {
  const state = useAppState();
  const { isLogin } = state;
  
  return isLogin ? <Redirect to="/home" /> : <Login />;
};

export default function App() {
  const classes = useStyles();
  return (
    <AppProvider>
      <Router>
        <div className={classes.root}>
          <CssBaseline />
          <Menu/>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <Switch>
              <Route exact path="/login" render={p => <LoginRender />} />
              <Route path="/about">
                <About />
              </Route>
              <Route path="/topics">
                <Topics />
              </Route>
              <Route path="/list">
                <Suspense fallback={<div>Loading...</div>}>
                  <List />
                </Suspense>
              </Route>
              <Route path="/photo">
                <Photo />
              </Route>
              <Route path="/swiper">
                <Swiper />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </main>
        </div>
      </Router>
    </AppProvider>
  );
}
