import React from "react";
import { render } from "@testing-library/react";

import { AppProvider } from "../context";

const Wrapper = ({ children } : {  children: React.ReactNode }) => {
  return (
    <AppProvider>
      {children}
    </AppProvider>
  );
};

const customRender = (ui: React.ReactElement, options?: any) =>
  render(ui, { wrapper: Wrapper, ...options });

export * from "@testing-library/react";

export { customRender as render };