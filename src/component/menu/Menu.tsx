import React from "react";
import clsx from "clsx";
import { Link } from "react-router-dom";
import { useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem, { ListItemProps } from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { links } from "./Links";
import { useStyles } from "./Styles";
import { useAppState, useAppDispatch } from "../../context"
import { LOGOUT } from "../../utils/const";

function ListItemLink(props: ListItemProps<"a", { button?: true }>) {
  return <ListItem button component="a" {...props} />;
}

const Menu = (() => {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const state = useAppState();
  const dispatch = useAppDispatch();
  const { isLogin } = state;

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleOnClick = () => {
    dispatch({ type: LOGOUT })
  }
  
  return (
    <>
      <AppBar position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}>
        <Toolbar>
          <IconButton
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
            onClick={handleDrawerOpen}
            color="inherit"
            data-testid="hamburger-menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Assignment
          </Typography>
          {
            isLogin ? (
              <Button onClick={handleOnClick} data-testid="login">LogOut</Button>
            ): (
              <Link to="/login" data-testid="login">Login</Link>
            )
          }
        </Toolbar>
      </AppBar>
      <Drawer
        data-testid="drawer-menu"
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        {
          links.map(link => (
            <List key={link.id}>
              <ListItem>
                <ListItemIcon>{link.icon}</ListItemIcon>
                <ListItemLink href={link.link}>
                  <ListItemText primary={link.name} />
                </ListItemLink>
              </ListItem>
            </List>
          ))
        }
      </Drawer>
    </>
  );
})

export default Menu;