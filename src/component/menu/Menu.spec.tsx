import React from "react";
import { MemoryRouter } from "react-router-dom";
import { render, fireEvent, screen } from "../../test-helper/custom-render";
import Menu from "./Menu";

describe("menu component", () => {
  beforeEach(() => {
    render(<MemoryRouter><Menu/></MemoryRouter>);
  })
  it("shows drawer menu when user click hambuger menu", async () => {
    const openMenuBtn = screen.getByTestId("hamburger-menu");

    fireEvent.click(openMenuBtn);

    const drawerMenu = (await screen.findByTestId("drawer-menu")).closest("div")

    expect(drawerMenu?.className).toMatch(/drawerOpen/)
  });
});