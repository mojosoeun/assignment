
import React from "react";
import HomeIcon from "@material-ui/icons/Home";
import PeopleIcon from "@material-ui/icons/People";
import InboxIcon from "@material-ui/icons/Inbox";
import AccessAlarmIcon from "@material-ui/icons/AccessAlarm";
import ThreeDRotation from "@material-ui/icons/ThreeDRotation";

export const links = [
  {
    id: "home",
    link: "/home",
    name: "Home",
    icon: <HomeIcon />
  },
  {
    id: "about",
    link: "/about",
    name: "About",
    icon: <PeopleIcon />
  },
  {
    id: "topics",
    link: "/topics",
    name: "Topics",
    icon: <InboxIcon/>
  },
  {
    id: "list",
    link: "/list",
    name: "List",
    icon: <AccessAlarmIcon />
  },
  {
    id: "photo",
    link: "/photo",
    name: "Photo",
    icon: <ThreeDRotation/>
  },
  {
    id: "swiper",
    link: "/swiper",
    name: "Swiper",
    icon: <HomeIcon />
  }
]