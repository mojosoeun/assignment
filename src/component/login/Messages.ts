export const EMAIL_ERR_MSG = "Email is not a valid email";
export const PW_ERR_MSG = "Password is too short (minimum is 8 characters)";