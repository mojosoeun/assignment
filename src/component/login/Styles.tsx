import { createStyles, makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: "flex",
      justifyContent: "center",
      flexDirection: "column",
      alignItems: "center"
    },
    title: {
      fontSize: "20px",
      fontWeight: "bold"
    },
    textField: {
      width: "600px",
      marginBottom: "10px"
    },
    submitBtn: {
      width: "600px"
    }
  }),
);