import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button"
import { useAppDispatch } from "../../context"
import { LOGIN }from "../../utils/const";
import { useStyles } from "./Styles"
import { EMAIL_ERR_MSG, PW_ERR_MSG } from "./Messages"

const Login = () => {
  const classes = useStyles();
  const dispatch = useAppDispatch();

  const [emailError, setEmailError] = useState({
    status: false,
    helperText: ""
  });

  const [pwdError, setPwdError] = useState({
    status: false,
    helperText: ""
  });

  const handleEmailOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const email = e.target.value;
    const re = /\S+@\S+\.\S+/;
    const isVerified = re.test(email);
    setEmailError({
      status: !isVerified,
      helperText: isVerified ? "" : EMAIL_ERR_MSG
    });
  };

  const handlePwdOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const p = e.target.value;
    const isVerified = p.length > 8;
    setPwdError({
      status: !isVerified,
      helperText: isVerified ? "" : PW_ERR_MSG
    })
  };

  const handleOnSubmit = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    dispatch({ type: LOGIN })
  }

  return (
    <form className={classes.root} onSubmit={handleOnSubmit}>
      <p className={classes.title}>Sign in</p>
      <TextField
          error={emailError.status}
          inputProps={{ "data-testid": "login-email" }}
          id="filled-email-input"
          label="Email"
          type="email"
          required={true}
          variant="outlined"
          helperText={emailError.helperText}
          className={classes.textField}
          onChange={handleEmailOnChange}
        />
      <TextField
          error={pwdError.status}
          inputProps={{ "data-testid": "login-pw" }}
          id="filled-password-input"
          label="Password"
          type="password"
          required={true}
          helperText={pwdError.helperText}
          autoComplete="current-password"
          variant="outlined"
          className={classes.textField}
          onChange={handlePwdOnChange}
        />
      <p>Fields that are marked with * sign are required.</p>
      <Button type="submit" variant="contained" color="primary" className={classes.submitBtn} data-testid="login-btn">Send</Button>
    </form>
  )
}

export default Login;