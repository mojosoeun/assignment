import React from "react";
import { MemoryRouter } from "react-router-dom";
import { render, fireEvent, screen } from "../../test-helper/custom-render";
import Login from "./Login";
import Menu from "../menu/Menu";

describe("login component", () => {
  beforeEach(() => {
    render(<MemoryRouter><Menu/><Login/></MemoryRouter>);
  })

  const doLogin = () => {
    const emailTextField = screen.getByTestId("login-email");
    const pwTextField = screen.getByTestId("login-pw");

    fireEvent.change(emailTextField, { target: { value: "hi@hey.com" } });
    fireEvent.change(pwTextField, { target: { value: "123456789" } })

    const loginBtn = screen.getByTestId("login-btn");

    fireEvent.click(loginBtn);
  }

  it("user can login", async () => {
    
    doLogin()

    const loginText = await screen.findByTestId("login");

    expect(loginText).toHaveTextContent("LogOut");

  });

  it("user can logout", async () => {

    doLogin()

    const loginText = await screen.findByTestId("login");
    fireEvent.click(loginText);

    const logOutText = await screen.findByTestId("login");

    expect(logOutText).toHaveTextContent("Login");

  })
});