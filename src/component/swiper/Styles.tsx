import { createStyles, makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() =>
    createStyles({
      root: {
        maxWidth: 800,
      },
      media: {
        height: 600,
        padding: 10,
      },

      swiperSection: {
        paddingBottom: "80px",
      },

      swiperContainer: {
        margin: "0 auto",
        position: "relative",
        overflow: "hidden",
      },

      swiperWrapper: {
        position: "relative",
        height: "100%",
        display: "flex",
        transition: "transform 0.3s"
      },

      swiperItem: {
        width: "700px",
        height: "400px",
        marginRight: "20px"
      },

      swiperImage: {
        pointerEvents: "none"
      },

      swiperInfo: {
        position: "relative",
        margin: "0px 24px 16px",
      },

      swiperInner: {
        position: "absolute",
        zIndex: 1,
        top: "-40px",
        left: "-1px",
        width: "100%",
        padding: "0px 4px 0px 8px",
        backgroundColor: "white",
        height: "120px",
      }
    })
  );