export const slides = [
  {
    id: "1",
    imageUrl: "https://via.placeholder.com/700x400.png?text=1",
    title: "스카이베이 호텔",
    desc: "강원 경포대 | 특 2급"
  },
  {
    id: "2",
    imageUrl: "https://via.placeholder.com/700x400.png?text=2",
    title: "인천 파크마린호텔",
    desc: "인천 인천시청 | 특2급"
  },
  {
    id: "3",
    imageUrl: "https://via.placeholder.com/700x400.png?text=3",
    title: "오라카이 청계산 호텔",
    desc: "서울 서초 | 특2급"
  }
]