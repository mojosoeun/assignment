import React, { useState, useRef, useEffect } from "react";
import {
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
} from "@material-ui/core";
import Card from "@material-ui/core/Card/Card";
import Typography from "@material-ui/core/Typography";
import { slides } from "./Slides"
import { useStyles } from "./Styles"

const Swiper: React.FC = () => {
  // fixme: Swiper 를 구현해 주세요.
  const classes = useStyles();

  const imgWrapperRef = useRef<HTMLDivElement>(null);
  const imgRef = useRef<HTMLDivElement>(null);

  const firstSlideX = 24;
  const totalImg = 3;
  const minimum_distance = 30;

  const [startX, setStartX] = useState(0);
  const [endX, setEndX] = useState(0);
  const [activeSlide, setActiveSlide] = useState({
    transform: `translateX(${firstSlideX}px)`
  });
  const [currentIndex, setCurrentIndex] = useState(0);
  const [pointerIsDown, setPointerIsDown] = useState(false);
  const [containerWidth, setContainerWidth] = useState(0);
  const [isMouseMoved, setIsMouseMoved] = useState(false);

  useEffect(() => {
    if (imgWrapperRef.current && imgRef.current) {
      setContainerWidth(imgRef.current.clientWidth * 0.97);
      imgWrapperRef.current.style.width = `${imgRef.current.clientWidth * totalImg}px`;
    }
  }, [])

  const switchImages = (distance: number) => {
    setActiveSlide({
      transform: `translateX(${distance}px)`
    });
  };

  const moveFirstSlide = () => {
    switchImages(firstSlideX);
  };

  const backOriginalSlide = () => {
    if (currentIndex === 0) {
      moveFirstSlide();
    } else {
      switchImages(-(currentIndex) * containerWidth)
    }
  };

  const moveRight = () => {
    return startX > endX && currentIndex < totalImg - 1;
  };

  const moveLeft = () => {
    return startX < endX && currentIndex > 0;
  };
 
  const handleMouseLeave = () => {
    if (!pointerIsDown) {
      return;
    }
    setPointerIsDown(false);
    setStartX(0);
    setEndX(0);
    moveFirstSlide()
  };

  const handleMouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    setPointerIsDown(true);
    setStartX(e.pageX);
  };

  const handleMouseUp = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    setPointerIsDown(false);

    const scrolled_distance = Math.abs(startX - endX);

    if(!isMouseMoved) {
      backOriginalSlide()
      return;
    }
    
    if (scrolled_distance < minimum_distance) {
      backOriginalSlide()
      return;
    }

    const isMoveRight = moveRight();
    const isMoveLeft = moveLeft();

    if (isMoveRight) {
      setCurrentIndex(currentIndex + 1);
      switchImages(-(currentIndex + 1) * containerWidth)
    } else if (isMoveLeft) {
      setCurrentIndex(currentIndex - 1);
      if (currentIndex - 1 === 0) {
        moveFirstSlide();
      } else {
        switchImages(-((currentIndex - 1) * containerWidth))
      }
    }

    setIsMouseMoved(false);
    
  };

  const handleMouseMove = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
    if(!pointerIsDown) {
      return;
    }
    setIsMouseMoved(true);
    setEndX(e.pageX);

    const isMoveRight = moveRight();
    const isMoveLeft = moveLeft();

    if (isMoveRight) {
      switchImages(-(currentIndex * containerWidth + endX));
    } else if (isMoveLeft) {
      switchImages(-(currentIndex * containerWidth - endX));
    }
  };

  return (
    <div className={classes.root}>
      <Card>
        <CardHeader title="아래 이미지와 같은 스와이퍼를 구현해주세요" />
        <CardMedia className={classes.media} image="./swiper.png" />
        <CardContent>
          <Typography paragraph>- 임의의 3개의 이미지로 작업 </Typography>
          <Typography paragraph>- 왼쪽 이미지가 살짝 보여야함</Typography>
          <Typography paragraph>
            - 이미지 하단부에 이미지에 관한 설명이 있어야함{" "}
          </Typography>
          <Typography paragraph>- www.dailyhotel.com 페이지 참고</Typography>
        </CardContent>
        <CardActions disableSpacing></CardActions>
      </Card>
      {/*여기서부터 구현하시면 됩니다*/}
      <div className={classes.swiperSection}>
        <div className={classes.swiperContainer}>
          <div ref={imgWrapperRef}
            data-testid="swiper-item"
            className={classes.swiperWrapper}
            style={activeSlide}
            onMouseUp={handleMouseUp}
            onMouseDown={handleMouseDown}
            onMouseMove={handleMouseMove}
            onMouseLeave={handleMouseLeave}>
            {
              slides.map((slide) => {
                return (
                  <div ref={imgRef} key={slide.id} className={classes.swiperItem}>
                    <img className={classes.swiperImage} src={slide.imageUrl} alt={slide.title} />
                  </div>
                )
              })
            }
          </div>
        </div>
        <div className={classes.swiperInfo}>
          <div className={classes.swiperInner}>
            <p data-testid="current-index">{`${currentIndex + 1} 번째 슬라이드`} </p>
            <p>{slides[currentIndex].title} </p>
            <p>{slides[currentIndex].desc} </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Swiper;