import React, { useEffect, useState } from "react";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import IconButton from "@material-ui/core/IconButton";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { Photos } from "./Types";
import { useStyles } from "./Styles";

const Photo: React.FC = () => {
  // fixme: grid-list 를 구현해주세요.
  // https://material-ui.com/components/grid-list/
  const classes = useStyles();
  const [photos, setPhotos] = useState<Photos>([]);
  
  useEffect(() => {
    //현재 제공하는 사진 api가 한번에 5000개씩 내려와 실제로 그리드를 그릴때 상당한 성능 이슈가 있습니다.
    //그래서, 일단은 임의로 5개만 그리도록 해 놓았습니다.
    //실제에서는 일정 갯수(예: 50개)씩 내려주고 그려주고, 추가하고 그려주고 하는 과정이 필요할것 같습니다.
    fetch("https://jsonplaceholder.typicode.com/photos")
      .then((response) => response.json())
      .then((json) => {
        json.length = 5
        setPhotos(json);
      });
  }, []);

  return (
    <div className={classes.root}>
      <GridList className={classes.gridList} cols={2.5}>
        {photos.map((photo) => (
          <GridListTile key={photo.id}>
            <img src={photo.thumbnailUrl} alt={photo.title} />
            <GridListTileBar
              title={photo.title}
              classes={{
                root: classes.titleBar,
                title: classes.title,
              }}
              actionIcon={
                <IconButton aria-label={`star ${photo.title}`}>
                  <StarBorderIcon className={classes.title} />
                </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </div>

  );
};

export default Photo;