type Photo = {
  id: number
  albumId: number
  title: string
  url: string
  thumbnailUrl: string
}

export type Photos = Photo[]