import React, { useEffect, useState } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Comments } from "./Types";

const List = () => {
  // fixme: error를 수정해 주세요
  // https://material-ui.com/components/tables/ 을 사용해서 comments를 테이블에 보여주세요
  const [list, setList] = useState<Comments>([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/comments")
      .then((response) => response.json())
      .then((json) => {
        setList(json);
      });
  }, []);

  return (
    <TableContainer component={Paper}>
      <Table aria-label="Comment List">
        <TableHead>
          <TableRow>
            <TableCell>id</TableCell>
            <TableCell>name</TableCell>
            <TableCell>email</TableCell>
            <TableCell>body</TableCell>
            <TableCell>postId</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {list.map((row) => (
            <TableRow key={row.id} data-testid="comment-table-item">
              <TableCell>{row.id}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.email}</TableCell>
              <TableCell>{row.body}</TableCell>
              <TableCell>{row.postId}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default List;