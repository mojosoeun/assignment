type Comment = {
  id: number
  name: string
  email: string
  body: string
  postId: number
}

export type Comments = Comment[]