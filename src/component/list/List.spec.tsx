import React from "react";
import { render, screen } from "../../test-helper/custom-render";
import List from "./List";

describe("list component", () => {
  beforeEach(() => {
    render(<List/>);
  })

  it("can render comment list", async () => {
    const commentItems = await screen.findAllByTestId("comment-table-item");

    expect(commentItems).toHaveLength(500);
  });
});