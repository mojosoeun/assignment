import React, { useReducer, useContext, createContext } from "react";
import { Dispatch } from "react";
import { LOGIN, LOGOUT } from "./utils/const";

export type InitialStateType = {
  isLogin: boolean
}

export type Action =
 | { type: "LOGIN" }
 | { type: "LOGOUT"}

export type InitialDispatch = Dispatch<Action>

const initialState = {
  isLogin: false
};
const initialDispatchFunc = () => {};

const InitialStateContext = createContext<InitialStateType>(initialState);
const InitialDispatchContext = createContext<InitialDispatch>(initialDispatchFunc);

function reducer(state: InitialStateType, action: Action): InitialStateType {
  switch (action.type) {
    case LOGIN:
      return {
        isLogin: true
      }
    case LOGOUT:
      return {
        isLogin: false
      }
    default:
      throw new Error("Unhandled action")
  }
}

export function AppProvider({ children } : {  children: React.ReactNode }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <InitialStateContext.Provider value={state}>
      <InitialDispatchContext.Provider value={dispatch}>
        {children}
      </InitialDispatchContext.Provider>
    </InitialStateContext.Provider>
  );
}

export function useAppState() {
  const state = useContext(InitialStateContext);
  return state;
}

export function useAppDispatch() {
  const state = useContext(InitialDispatchContext);
  return state;
}
